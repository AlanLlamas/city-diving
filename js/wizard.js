(function (){

	
	var steps = angular.module('wizard', []);
	steps.controller('StepsCtrl', ['$http','$log', function ($http,$log) {
		var date = [];
	 	var	considerations = {kids:false,pets:false,health:false,healthData:"",others:false,othersData:""};
	 	var activities = [];
	 	var dateHas;
	 	var nameHas;
	 	var emailHas;
	 	var validate;
	 	//var hours;
		this.step = 0;
		var	tour = {
				 date: date,
				 hours: '',
				 people: '2',
				 activities: activities,
				 considerations: considerations,
				 forgot: '',
				 name: '',
				 email: ''
				 
		};
	
		this.tour = tour;
		this.dateHas = dateHas;
 	

		this.store = function  () {
			if (!store.enabled) {
				alert('Local storage is not supported by your browser. Please disable "Private Mode", or upgrade to a modern browser.')
				return
			}
		}
		this.validateEmail = function (email){
		    var re = /\S+@\S+\.\S+/;
			validate = re.test(email);
		    return validate ;
		};
		//comprobaciones si hay datos se popula el wizard
		if (store.get('stage') != undefined && store.get('stage') != 8) {
			this.step = store.get('stage');
		};
		if (store.get('date') != undefined && date.length == 0) {
			date.push(store.get('date'))
		};
		if (store.get('hours') != undefined && tour.hours == ''){
			tour.hours = store.get('hours');
		}
		getAct = store.get('activities');
				
		if (getAct != undefined) {
			for (var i = 0; i < getAct.length; i++) {
			index = activities.indexOf(getAct[i]);
			activitie = $('' + getAct[i]);

				if (index == -1) {
					activities.push(getAct[i]);
					activitie.addClass("selected");
				};
			};
		};
		if (store.get('people') != undefined) {
			tour.people = store.get('people');
		};
		cons = store.get('considerations');
		if (cons != undefined) {
			considerations.kids       = cons.kids;
			considerations.pets       = cons.pets;
			considerations.health     = cons.health;
			considerations.healthData = cons.healthData;
			considerations.others     = cons.others;
			considerations.othersData = cons.othersData;
			
		};
		if (store.get('forgot') != undefined) {
			tour.forgot = store.get('forgot');
		};
		this.currStep = function (curr) {


			//manejo de steps y logica del wizard
				if (curr == 1) {
					return this.step = curr;
				};
			//comprobacion date
				if (this.step == 1 && curr == 2) {
					$('.date').empty();
					dateHas = $('.datepicker').hasClass('error').toString();
					edate = store.get('date');
					if (edate != null && edate != "" && date.length === 0) {
					 	date.push(edate)
					}else if (edate != null && edate != ""){
					
						date.splice(0,1)
						date.push(edate)
					}else{
						date = [];
					};
					store.remove('date')
					
					if (date.length === 1 && date[0] != null ) {
						store.set('date', edate)
						store.set('stage', curr)										
						return this.step = curr;
					}else{
						$('.datepicker').addClass("error");
						if (dateHas) {
							$('.date').append("Please choose a date to continue.")
						};
					};
				}else if (this.step == 3 && curr == 2) {
						return this.step = curr;				
				};
			//comprobacion hours
				if (curr == 3 && store.get('hours') == undefined && tour.hours == '') {
					$('.hourerr').empty();
					$('.hourerr').append('Please select the hours you want to spend with us.')
					}else if (curr == 3  && tour.hours != '') {

						store.set('hours',tour.hours)
						store.set('stage',curr)										

						return this.step = curr;
				};
			//comprobacion people
				if (curr == 4) {
					store.set('people',tour.people)
					store.set('stage',curr)
					

						return this.step = curr;
					
				};
			//comprobacion activities
				if (this.step == 4 && curr == 5) {

					if (tour.hours == 4 && tour.activities.length == 2) {
					store.set('activities', tour.activities)
					store.set('stage',curr)								

						return this.step = curr;
					};
					if (tour.hours == 8 && tour.activities.length == 3) {
						store.set('activities', tour.activities)
						store.set('stage',curr)										
						return this.step = curr;	
					};
				}else if (this.step == 6 && curr == 5) {
					return this.step = curr;
				};
			//comprobacion en considerations
				if (curr == 6 ) {
					cons = tour.considerations;
					if (cons.health && cons.healthData == '' && cons.others  && cons.othersData == '') {
						$('.pcons').addClass('error');
						$('.pcons').empty();
						$('.pcons').append('Please specify your health problems and other data to continue.')	
					}else if (cons.health && cons.healthData != '' && cons.others && cons.othersData != '') {
						$('.pcons').empty();
						store.set('considerations', cons)
						store.set('stage',curr)										
						return this.step = curr;
					}else if (cons.health  && cons.healthData == '') {
						$('.pcons').addClass('error');
						$('.pcons').empty();
						$('.pcons').append('Please specify your health problems to continue.')
					}else if (cons.others && cons.othersData == '') {
						$('.pcons').addClass('error');
						$('.pcons').empty();
						$('.pcons').append('Please specify your other data to continue.')
					}else if (cons.health && cons.healthData != '') {
						$('.pcons').empty();
						store.set('considerations', cons)
						store.set('stage',curr)										
						return this.step = curr;
					}else if (cons.others  && cons.othersData != '') {
						$('.pcons').empty();
						store.set('considerations', cons)
						store.set('stage',curr)										
						return this.step = curr;
					}else {
						$('.pcons').empty();
						store.set('considerations', cons)
						store.set('stage',curr)										
						return this.step = curr;
					};

				};
			//comprobacion no necesaria en forgot
				if (curr == 7) {
					store.set('forgot', tour.forgot)
					store.set('stage',curr)								
					return this.step = curr;
				};
			//comprobacion de nombre email y envio de datos
				if (curr == 8 && tour.name == '' && tour.email == '') {
					$('.pname').empty();
					nameHas = $('.name').hasClass('error').toString();
					emailHas = $('.email').hasClass('error').toString();
					
					$('.name').addClass("error");
					$('.email').addClass("error");
					if (nameHas && emailHas) {
							$('.pname').append("Please provide us your name and email to continue.")
						};

				}else if (curr == 8 && tour.name == '') {
					$('.pname').empty();
					nameHas = $('.name').hasClass('error').toString();
					$('.name').addClass("error");
					if (nameHas) {
						$('.pname').append("Please provide us your name to continue.")
					};
			
				}else if (curr == 8 && tour.email == '') {
					$('.pname').empty();
					emailHas = $('.email').hasClass('error').toString();
					$('.email').addClass("error");
					if (emailHas) {
							$('.pname').append("Please provide us your email to continue.")
						};
				}else if (curr == 8 && validate == false) {
					$('.pname').empty();
					emailHas = $('.email').hasClass('error').toString();
					$('.email').addClass("error");
					if (emailHas) {
							$('.pname').append("Please provide us a valid email to continue.")
						};
				};
				if (curr == 8 && tour.name != '' && validate == true) {
					$('.name').removeClass("error");
					$('.email').removeClass("error");
					$('.pname').empty();
					store.set('name',tour.name)				
					store.set('email', tour.email)				
					sendTour= store.getAll()		
					$http.post("/", sendTour).then(function(success) {
						if (success.data === "0") {
							
							store.clear()
							$('.wizard').empty();
							$('.wizard').append("<h1>Your tour is looking good! We'll get back to you in less than 24 hours</h1>");
							
						}else{
							$('.pname').empty();
							$('.pname').append("Something went wrong try to send your tour later");
						};
					
					});
						
				};
		};

		this.addActivities = function (act) {
			this.activitie = act;
			index = activities.indexOf(this.activitie);
			activitie = $('' + this.activitie)
				
				if (index != -1) {
					activities.splice(index,1)
	
				}else if ( tour.hours == 4 && activities.length <= 1) { 

					activities.push(this.activitie)
					activitie.addClass("selected");
					
				}else if (tour.hours == 8 && activities.length <= 2 ) {
					 
					activities.push(this.activitie)
					activitie.addClass("selected");
				};
	
		};
	}])
	steps.directive('start', [function () {
		return {
			restrict: 'E',
			templateUrl: 'start.html'			
			
		};
	}]);
	steps.directive('date', [ function () { 
		return {
			
			restrict: 'E',
			templateUrl: 'date.html',
			
				controller: function (){
					$('.datepicker').datepicker({
						gotoCurrent: true,
						dateFormat: 'dd/MM/yy',
						 minDate: "+3d",
						onClose: function  (date) {
							store.set('date', date)
							return date ;
						}

					});
					
				},
				
				controllerAs: 'datepicker'
		};

	}]);
	steps.directive('hours', [function () {
		return {
			restrict: 'E',
			templateUrl: 'hours.html'
		};
	}]);
	steps.directive('people', [function () {
		return {
			restrict: 'E',
			templateUrl: 'people.html'			
			
		};
	}]);
	steps.directive('day', [function () {
		return {
			restrict: 'E',
			templateUrl: 'day.html'			
			
		};
	}]);
	steps.directive('activities', [function () {
		return {
			restrict: 'E',
			templateUrl: 'activities.html'			
		
		};
	}]);
	steps.directive('considerations', [function () {
		return {
			restrict: 'E',
			templateUrl: 'considerations.html'			
			
		};
	}]);
	steps.directive('forgot', [function () {
		return {
			restrict: 'E',
			templateUrl: 'forgot.html',
			controller: function() {
				$('.animated').effect("shake","slow");
			},
			controllerAs: 'forgotCtrl'		

			
		};
	}]);
	steps.directive('name', [function () {
		return {
			restrict: 'E',
			templateUrl: 'name.html'			
			
		};
	}]);
	steps.directive('end', [function () {
		return {
			restrict: 'E',
			templateUrl: 'end.html'			
			
		};
	}]);

	

})();
