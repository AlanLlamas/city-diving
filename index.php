<?php
	if (strtoupper($_SERVER['REQUEST_METHOD']) == 'POST'){
		echo "0";
	} else {
?>
<!DOCTYPE html>
<html ng-app="wizard" lang="en-US">
<head>
	<meta charset="UTF-8" >
	<title>City Diving</title>
	<link rel="stylesheet" href="http://code.jquery.com/ui/1.11.2/themes/smoothness/jquery-ui.css">

</head>
<body class="wizard" >
	<form name="myForm" novalidate ng-controller="StepsCtrl as steps" ng-switch = "steps.step"   >
		<start ng-switch-when ="0"></start>
		<date ng-switch-when ="1"></date>
		<hours ng-switch-when ="2"></hours>
		<people ng-switch-when ="3"></people>
		<activities ng-switch-when ="4"></activities>
		<considerations ng-switch-when ="5"></considerations>
		<forgot ng-switch-when ="6"> </forgot>
		<name ng-switch-when ="7"> </name>
	
		<p ng-hide="steps.step == 0" >step {{steps.step}} of 7</p>
	</form>
	<script type="text/javascript" src="js/angular.js"></script>
	<script src="js/store.min.js"></script>
	<script type="text/javascript" src="js/wizard.js"></script>
	<script type="text/javascript" src="http://code.jquery.com/jquery-1.11.2.min.js"></script>
	<script type="text/javascript" src="http://code.jquery.com/ui/1.11.2/jquery-ui.js"></script>
	
	
</body>
</html>
<?php
	}
?>
